const books = require('../models/books.js')
const category = require('../models/category.js')
const catalogs = require('../models/catalog.js')
const fs = require('fs')
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

books.belongsTo(category)

module.exports.getAllBooks = (req, res) => {
    books.findAll().then((books) => {
        res.json(books)
    })
}
module.exports.getFindBukuId = (req, res) =>{
	books.findByPk(req.params.id).then(buku => {
		res.json(buku)
	})
} 

module.exports.createBooks = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
        books.create({
            judul_buku: req.body.judul_buku,
            pengarang: req.body.pengarang,
            penerbit: req.body.penerbit,
            jumlah_hal: req.body.jumlah_hal,
            harga: req.body.harga,
            deskripsi_buku: req.body.deskripsi_buku,
            tanggal_rilis: req.body.tanggal_rilis,
            categoryId: req.body.id_kategori,
            catalogId: req.body.id_katalog
    }).then((books) => {
        res.json(books)
    })
}
        }
    })
}

module.exports.updateBooks = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
				books.update({
                    judul_buku: req.body.judul_buku,
                    pengarang: req.body.pengarang,
                    penerbit: req.body.penerbit,
                    jumlah_hal: req.body.jumlah_hal,
                    harga: req.body.harga,
                    deskripsi_buku: req.body.deskripsi_buku,
                    tanggal_rilis: req.body.tanggal_rilis,
                    categoryId: req.body.id_kategori,
                    catalogId: req.body.id_katalog
						}, {
  						where: { 
								Id: req.params.id
								}
						}).then(buku => {
							res.json("Data Berhasil di Perbaharui");
						});
			}
			else{
				res.sendStatus(403);
			}
		}
	}
)}



module.exports.deleteBooks = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
				books.destroy({
					where: {
						Id: req.params.id
					}
				})
				.then(function (deletedRecord) {
					if(deletedRecord === 1){
						res.status(200).json({message:"Deleted Successfully"});          
					}
					else
					{
						res.status(404).json({message:"Record Not Found"})
					}
				})
				.catch(function (error){
					res.status(500).json(error);
				});
			}else{
				res.sendStatus(403);
			}
		}   
	})
}       