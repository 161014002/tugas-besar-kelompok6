const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
const Keranjang = require('../models/keranjang');

module.exports.getAll= (req, res) =>{
	Keranjang.findAll().then(Keranjang=> {
		res.json(Keranjang);
	}).catch((error)=>{
		console.log(error);
	});
}

module.exports.postAddKeranjang = (req, res) =>{
		jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
				if (error) {
						res.json("Token Tidak Cocok");
				}else{
					if (authData['roles']=="user") {
						var id_buku = req.body.id_buku;
						Keranjang.create({
								id_user : authData['id'],
								id_buku : id_buku
						})
						.then(Keranjang => {
								res.json(Keranjang);
						});
				}else{
					res.json("Anda harus login sbagai user jangan sebagai admin");
				}
			}
		});
}