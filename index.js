const express = require('express')
const app = express()
const port = 6969
const bodyParser = require('body-parser')
const path = require('path')
const router = require('./routes/router')
const sequelize = require('./configs/db_sequelize.js')

app.use(
    bodyParser.urlencoded({
        extended: false
    })
)

app.use( express.static(__dirname + '/public'));

app.get('/api', (req, res) => {
    res.render('api')
})


app.use(bodyParser.json())

app.set('view engine', 'ejs')

app.use(router)

app.listen(port, () => {
    console.log(`Port Berjalan Di 6969`)
    sequelize.sync()
})