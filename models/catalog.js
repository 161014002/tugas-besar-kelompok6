const Sequelize = require ('sequelize')
const sequelize = require ('../configs/db_sequelize')

class Catalog extends Sequelize.Model{}

Catalog.init({
    name_catalog: Sequelize.STRING,
},{
    sequelize,
    modelName: 'catalog'
})

module.exports = Catalog;