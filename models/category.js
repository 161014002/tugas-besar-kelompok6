const Sequelize = require ('sequelize')
const sequelize = require ('../configs/db_sequelize')

class Category extends Sequelize.Model{}

Category.init({
    name_category: Sequelize.STRING,
},{
    sequelize,
    modelName: 'category'
})

module.exports = Category;