const Sequelize = require ('sequelize')
const sequelize = require ('../configs/db_sequelize')

class Admin extends Sequelize.Model{}

Admin.init({
    name: Sequelize.STRING,
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
},{
    sequelize,
    modelName: 'admin'
})

module.exports = Admin;