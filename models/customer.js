const Sequelize = require('sequelize');

const sequelize = require('../configs/db_sequelize');

class Customer extends Sequelize.Model {}

Customer.init({
    name: Sequelize.STRING,
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING
}, { 
  sequelize,
   modelName: 'customer' 
});

module.exports = Customer;