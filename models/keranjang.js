const Sequelize = require ('sequelize')
const sequelize = require ('../configs/db_sequelize')

class Keranjang extends Sequelize.Model{}

Keranjang.init({
    id_user: Sequelize.INTEGER,
    id_buku: Sequelize.INTEGER
},{
    sequelize,
    modelName: 'keranjang'
})

module.exports = Keranjang;