const Sequelize = require ('sequelize')
const sequelize = require ('../configs/db_sequelize')


class Book extends Sequelize.Model{}

Book.init({
    judul_buku: Sequelize.STRING,
    pengarang: Sequelize.STRING,
    penerbit: Sequelize.STRING,
    jumlah_hal: Sequelize.STRING,
    harga: Sequelize.INTEGER,
    deskripsi_buku: Sequelize.STRING,
    tanggal_rilis: Sequelize.DATE
},{
    sequelize,
    modelName: 'book'
})

module.exports = Book;