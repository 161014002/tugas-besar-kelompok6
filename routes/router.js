const express = require('express')
const app = express.Router()
const booksController = require('../controllers/booksController.js')
const customerController = require('../controllers/userController.js')
const categoryController = require('../controllers/categoryController.js')
const keranjangController = require('../controllers/chartController');
const auth = require('../configs/auth.js')


/*
Baris Ini diisi oleh instansiasi model
*/

//Masuk Ke Koding Router

app.get('/', (req, res) => {
    res.render('index.ejs')
})

app.get('/order', keranjangController.getAll);
app.post('/order',auth.verifyToken, keranjangController.postAddKeranjang);

app.post('/user/register', customerController.postDaftar)
app.post('/user/login', customerController.postMasuk)

app.get('/books', booksController.getAllBooks)
app.get('/books/:id', booksController.getFindBukuId)
app.post('/books/create',auth.verifyToken,booksController.createBooks)
app.put('/books/update/:id',auth.verifyToken,booksController.updateBooks)
app.delete('/books/delete/:id',auth.verifyToken,booksController.deleteBooks)


app.get('/books/category', categoryController.getAllCategory)
app.post('/books/category/create', categoryController.createCategory)
app.put('/books/category/update', categoryController.updateCategory)
app.delete('/books/category/delete', categoryController.deleteCategory)

module.exports = app